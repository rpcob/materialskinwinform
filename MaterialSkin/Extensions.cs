﻿namespace MaterialSkin
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Reflection;

    /// <summary>
    /// Defines the <see cref="Extensions" />
    /// These add functions on default C# types and classes
    /// </summary>
    internal static class Extensions
    {
        /// <summary>
        /// The HasProperty
        /// </summary>
        /// <param name="objectToCheck">The objectToCheck<see cref="object"/></param>
        /// <param name="propertyName">The propertyName<see cref="string"/></param>
        /// <returns>The <see cref="bool"/></returns>
        public static bool HasProperty(this object objectToCheck, string propertyName)
        {
            try {
                var type = objectToCheck.GetType();

                return type.GetProperty(propertyName) != null;
            }
            catch (AmbiguousMatchException) {
                // ambiguous means there is more than one result,
                // which means: a method with that name does exist
                return true;
            }
        }

        /// <summary>
        /// The IsMaterialControl
        /// </summary>
        /// <param name="obj">The obj<see cref="Object"/></param>
        /// <returns>The <see cref="bool"/></returns>
        public static bool IsMaterialControl(this Object obj)
        {
            if (obj is IMaterialControl) {
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Transforms every char on a string to this dot: ●
        /// </summary>
        /// <param name="plainString"></param>
        /// <returns></returns>
        public static string ToSecureString(this string plainString)
        {
            if (plainString == null) {
                return null;
            }

            string secureString = "";
            for (uint i = 0; i < plainString.Length; i++) {
                secureString += '\u25CF';
            }
            return secureString;
        }

        // Color extensions
        public static Color ToColor(this int argb)
        {
            return Color.FromArgb(
                (argb & 0xFF0000) >> 16,
                (argb & 0x00FF00) >> 8,
                 argb & 0x0000FF);
        }

        public static Color RemoveAlpha(this Color color)
        {
            return Color.FromArgb(color.R, color.G, color.B);
        }

        public static int PercentageToColorComponent(this int percentage)
        {
            return (int)((percentage / 100d) * 255d);
        }

        public static unsafe Image ReplaceColor(this Image source, Color toReplace, Color replacement)
        {
            var bmp = new Bitmap(source);

            return (Image)bmp.ReplaceColor(toReplace, replacement);
        }

        public static unsafe Bitmap ReplaceColor(this Bitmap source, Color toReplace, Color replacement)
        {
            const int pixelSize = 4; // 32 bits per pixel

            Bitmap target = new Bitmap(
              source.Width,
              source.Height,
              PixelFormat.Format32bppArgb);

            BitmapData sourceData = null, targetData = null;

            try {
                sourceData = source.LockBits(
                  new Rectangle(0, 0, source.Width, source.Height),
                  ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                targetData = target.LockBits(
                  new Rectangle(0, 0, target.Width, target.Height),
                  ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

                for (int y = 0; y < source.Height; ++y) {
                    byte* sourceRow = (byte*)sourceData.Scan0 + (y * sourceData.Stride);
                    byte* targetRow = (byte*)targetData.Scan0 + (y * targetData.Stride);

                    for (int x = 0; x < source.Width; ++x) {
                        byte b = sourceRow[x * pixelSize + 0];
                        byte g = sourceRow[x * pixelSize + 1];
                        byte r = sourceRow[x * pixelSize + 2];
                        byte a = sourceRow[x * pixelSize + 3];

                        if (toReplace.R == r && toReplace.G == g && toReplace.B == b) {
                            r = replacement.R;
                            g = replacement.G;
                            b = replacement.B;
                        }

                        targetRow[x * pixelSize + 0] = b;
                        targetRow[x * pixelSize + 1] = g;
                        targetRow[x * pixelSize + 2] = r;
                        targetRow[x * pixelSize + 3] = a;
                    }
                }
            }
            finally {
                if (sourceData != null) {
                    source.UnlockBits(sourceData);
                }

                if (targetData != null) {
                    target.UnlockBits(targetData);
                }
            }

            return target;
        }
    }

    public enum DateRangeType
    {
        TODAY,
        YESTERDAY,
        THISWEEK,
        LASTWEEK,
        THISMONTH,
        LASTMONTH,
        THISYEAR,
        LASTYEAR,
        CUSTOM
    }

    public enum ControlSize
    {
        SMALL,
        NORMAL,
        LARGE
        //EXTRA_LARGE
    }
}